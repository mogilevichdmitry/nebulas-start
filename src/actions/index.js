export const SET_PB_KEY = 'SET_PB_KEY';

export const setPbKey = (pbKey) => {
  return {
    type: SET_PB_KEY,
    payload: {
      pbKey,
    }
  }
}