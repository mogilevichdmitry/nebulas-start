import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setPbKey } from '../../actions/index';
import CreateMnemonic from '../../components/create-mnemonic/create-mnemonic.component';

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    setPbKey,
  }, dispatch),
})

export const CreateMnemonicContainer = connect(null, mapDispatchToProps)(CreateMnemonic);
