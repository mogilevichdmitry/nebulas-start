import { connect } from 'react-redux';
import Home from '../../components/home/home.component';

const mapStateToProps = (state, ownProps) => ({
  pbKey: state.pbKey.value,
})

export const HomeContainer = connect(mapStateToProps)(Home);
