import { connect } from 'react-redux';
import Header from '../../components/header/header.component';

const mapStateToProps = (state, ownProps) => ({
  pbKey: state.pbKey.value,
})

export const HeaderContainer = connect(mapStateToProps)(Header);
