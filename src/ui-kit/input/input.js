import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './input.css';

class Input extends Component {
  static defaultProps = {
    type: 'text',
    isReadOnly: false,
  };

  render() {
    const { value, onChange, placeholder, type, isReadOnly } = this.props;

    return (
      <input className="input" readOnly={isReadOnly} type={type} value={value} placeholder={placeholder} onChange={onChange} />
    );
  }
}

Input.propTypes = {
  type: PropTypes.string,
  value: PropTypes.oneOf([PropTypes.number, PropTypes.string]),
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  isReadOnly: PropTypes.bool,
};

export default Input;