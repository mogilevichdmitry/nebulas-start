import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import './button.css';

class Button extends Component {
  static defaultProps = {
    isAlternative: false,
  }

  render() {
    const { className, children, isDisabled, onClick, isAlternative } = this.props;

    return (
      <button
        className={cn('button', className, {
          'button_alt': isAlternative,
        })}
        disabled={isDisabled}
        onClick={onClick}
      >
        {children}
      </button>
    )
  }
}

Button.propTypes = {
  className: PropTypes.string,
  isDisabled: PropTypes.bool,
  children: PropTypes.node,
  onClick: PropTypes.func,
  isAlternative: PropTypes.bool,
}

export default Button;