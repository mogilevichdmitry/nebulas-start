import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './textarea.css';

class TextArea extends Component {
  static defaultProps = {
    isReadOnly: false,
  }

  render() {
    const { children, placeholder, onChange, isReadOnly } = this.props;

    return (
      <textarea
        className="textarea"
        placeholder={placeholder}
        onChange={onChange}
        readOnly={isReadOnly}
        value={children}
      />
    );
  }
}

TextArea.propTypes = {
  children: PropTypes.node,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  isReadOnly: PropTypes.bool,
}

export default TextArea;
