import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './selectbox.css';

class Selectbox extends Component {
  render() {
    const { children } = this.props;

    return (
      <div className="selectbox">
        <button className="selectbox__trigger">
          {children}
        </button>
      </div>
    )
  }
}

Selectbox.propTypes = {
  children: PropTypes.node,
}

export default Selectbox;
