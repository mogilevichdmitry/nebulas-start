import { combineReducers } from 'redux';
import { SET_PB_KEY } from '../actions/index';

const pbKey = (state = {}, action) => {
  switch(action.type) {
    case SET_PB_KEY: 
      return {
        ...state,
        value: action.payload.pbKey,
      }
    default: 
      return state;
  }
}

const rootReducer = combineReducers({
  pbKey,
});

export default rootReducer;