import React, { Component } from 'react';
import './loader.component.css';

class Loader extends Component {
  render() {
    return (
      <div className="loader" />
    );
  }
}

export default Loader;