import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link, Switch } from 'react-router-dom';
import { Route } from 'react-router';
import { CreateMnemonicContainer as CreateMnemonic } from '../../containers/create-mnemonic/create-mnemonic.container';
import './create.component.css';

class Create extends Component {
  render() {
    return (
      <div className="create">
        <Switch>
          <Route exact path="/create">
            <Fragment>
              <h2 className="create__title">Select create method</h2>
              <div className="create__methods">
                <div className="create__method">
                  <span className="create__method-text">Generate Mnemonic Phrase</span>
                  <span className="create__method-hint">A string of twelve words</span>
                  <Link className="create__method-button" to="/create/mnemonic">Create new wallet via mnemonic</Link>
                </div>
                <div className="create__method">
                  <span className="create__method-text">Generate Keystore File</span>
                  <span className="create__method-hint">An encrypted JSON file, protected by a password</span>
                  <Link className="create__method-button" to="/create/keystore">Create new wallet via file</Link>
                </div>
              </div>
            </Fragment>
          </Route>
          <Route path="/create/mnemonic" component={CreateMnemonic} />
          <Route path="/create/keystore" component={CreateMnemonic} />
        </Switch>
      </div>
    );
  }
}

Create.propTypes = {
  setPbKey: PropTypes.func,
}

export default Create;