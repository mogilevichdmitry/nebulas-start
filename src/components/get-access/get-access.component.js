import React, { Component, Fragment } from 'react';
import { Link, Switch } from 'react-router-dom';
import { Route } from 'react-router';
import GetAccessMnemonic from '../get-access-mnemonic/get-access-mnemonic.component';
import GetAccessKeystore from '../get-access-keystore/get-access-keystore.component';
import './get-access.component.css';

class GetAccess extends Component {
  render() {
    return (
      <div className="access">
        <Switch>
          <Route exact path="/get-access">
            <Fragment>
              <h2 className="access__title">Select access method</h2>
              <div className="access__methods">
                <div className="access__method">
                  <span className="access__method-text">Mnemonic Phrase</span>
                  <span className="access__method-hint">You must enter a string of twelve words</span>
                  <Link className="access__method-button" to="/get-access/mnemonic">Get acces via mnemonic</Link>
                </div>
                <div className="access__method">
                  <span className="access__method-text">Keystore File</span>
                  <span className="access__method-hint">Need to load keystore file (JSON)</span>
                  <Link className="access__method-button" to="/get-access/keystore">Get acces via file</Link>
                </div>
              </div>
            </Fragment>
          </Route>
          <Route path="/get-access/mnemonic" component={GetAccessMnemonic} />
          <Route path="/get-access/keystore" component={GetAccessKeystore} />
        </Switch>
      </div>
    )
  }
}

export default GetAccess;