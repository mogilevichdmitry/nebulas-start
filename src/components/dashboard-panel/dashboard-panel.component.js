import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import './dashboard-panel.component.css';

class DashboardPanel extends Component {
  render() {
    const { backButton } = this.props;
    return (
      <div className="dashboard-panel">
        {backButton}
        <Link className="dashboard-panel__app dashboard-panel__app_wallet" to="/dashboard/wallet">
          W
        </Link>
        <Link className="dashboard-panel__app dashboard-panel__app_market" to ="/dashboard/market">
          M
        </Link>
      </div>
    );
  }
}

DashboardPanel.propTypes = {
  backButton: PropTypes.node,
}

export default DashboardPanel;