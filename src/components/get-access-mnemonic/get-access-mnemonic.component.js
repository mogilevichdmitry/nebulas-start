import React, { Component } from 'react';
import Button from '../../ui-kit/button/button';
import TextArea from '../../ui-kit/textarea/textarea';
import './get-access-mnemonic.component.css';

class GetAccessMnemonic extends Component {
  state = {
    isAccepted: false,
  }

  render() {
    const { isAccepted } = this.state;

    return (
      <div className="access-mnemonic">
        <h2 className="access-mnemonic__title">Type you mnemonic phrase below</h2>
        <TextArea placeholder="Mnemonic Phrase" />
        <div className="access-mnemonic__footer">
          <label className="access-mnemonic__label">
            <input type="checkbox" onChange={this.handleCheckbox} />
            <span className="access-mnemonic__label-hint">
              I have checked the URL to make sure this is the real https://start.nebulas.io/
            </span>
          </label>
          <Button isDisabled={!isAccepted}>Get Access</Button>
        </div>
      </div>
    );
  }

  handleCheckbox = (e) => {
    this.setState({
      isAccepted: e.target.checked,
    });
  }
}

export default GetAccessMnemonic;