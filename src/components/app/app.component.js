import React, { Component, Fragment } from 'react';
import { HeaderContainer as Header } from '../../containers/header/header.container';
import { HomeContainer as Home } from '../../containers/home/home.container';
import { Route, withRouter } from 'react-router';
import { Switch } from 'react-router-dom';
import DashboardApp from '../dashboard-app/dashboard-app.component';
import DashboardPanel from '../dashboard-panel/dashboard-panel.component';
import GetAccess from '../get-access/get-access.component';
import Create from '../create/create.component';
import './app.component.css';

class App extends Component {
  render() {
    return (
      <div className="app">
        <Header />
        <div className="app__body">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/get-access">
              <Fragment>
                <div className="app__back-button-wrapper">{this.renderBackButton('Back')}</div>
                <GetAccess />
              </Fragment>
            </Route>
            <Route path="/create">
              <Fragment>
                <div className="app__back-button-wrapper">{this.renderBackButton('Back')}</div>
                <Create />
              </Fragment>
            </Route>
            <Route path="/dashboard/:appName">
              <Fragment>
                <DashboardPanel backButton={this.renderBackButton('Back to dashboard')} />
                <DashboardApp />
              </Fragment>
            </Route>
          </Switch>
        </div>
      </div>
    )
  }

  renderBackButton = (title) => (
    <button className="app__back-button" onClick={() => this.props.history.goBack()}>
      <span className="app__back-button-arrow">←</span>
      <span>{title}</span>
    </button>
  );
};

export default withRouter(App);