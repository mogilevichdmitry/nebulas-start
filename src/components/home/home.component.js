import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import Dashboard from '../dashboard/dashboard.component';
import PropTypes from 'prop-types';
import './home.component.css';

class Home extends Component {
  render() {
    const { pbKey } = this.props;

    return (
      <Fragment>
        {!pbKey && (
          <div className="home">
            <Link to="/get-access" className="home__item home__item_entry">
              <span className="home__item-text">Get access to my wallet</span>
            </Link>
            <Link to="/create" className="home__item home__item_create">
              <span className="home__item-text">Create new wallet</span>
            </Link>
          </div>
        )}
        {pbKey && (
          <Dashboard />
        )}
      </Fragment>
    );
  }
};

Home.propTypes = {
  pbKey: PropTypes.string,
}

export default Home;
