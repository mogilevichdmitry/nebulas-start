import React, { Component, Fragment } from 'react';
import Loader from '../loader/loader.component';
import './dashboard-app.component.css';

class DashboardApp extends Component {
  render() {
    // todo title 
    return (
      <Fragment>
        <Loader />
        <div className="dashboard-app">
          <iframe className="dashboard-app__iframe" src="https://coinmarketcap.com/" />
        </div>
      </Fragment>
    )
  }
}

export default DashboardApp;