import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Selectbox from '../../ui-kit/selectbox/selectbox';
import PropTypes from 'prop-types';
import './header.component.css';

class Header extends Component {
  render() {
    const { pbKey } = this.props;

    return (
      <div className="header">
        <Link to="/" className="header__logo" />
        <div className="header__aside">
          {pbKey && 
            <div className="header__pbKey">
              <span className="header__pbKey-hint">Public key: </span>
              <span className="header__pbKey-value">{pbKey}</span>
            </div>
          }
          <Selectbox>
            <span className="header__selectbox-name">NAS</span>
            <span className="header__selectbox-url">(nebulas.io)</span>
            <span className="header__selectbox-arrow">▼</span>
          </Selectbox>
        </div>
      </div>
    )
  }
}

Header.propTypes = {
  pbKey: PropTypes.string,
};

export default Header;