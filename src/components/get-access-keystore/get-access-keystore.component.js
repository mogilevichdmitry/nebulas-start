import React, { Component } from 'react';
import Button from '../../ui-kit/button/button';
import Input from '../../ui-kit/input/input';
import './get-access-keystore.component.css';

class GetAccessKeystore extends Component {
  state = {
    isAccepted: false,
  }

  render() {
    const { isAccepted } = this.state;

    return (
      <div className="access-keystore">
        <h2 className="access-keystore__title">Upload keystore file (JSON)</h2>
        <Input placeholder="Click to attach file" isReadOnly={true}/>
        <input className="access-keystore__input-file" type="file" />
        <div className="access-mnemonic__footer">
          <label className="access-keystore__label">
            <input type="checkbox" onChange={this.handleCheckbox} />
            <span className="access-keystore__label-hint">
              I have checked the URL to make sure this is the real https://start.nebulas.io/
            </span>
          </label>
          <Button isDisabled={!isAccepted}>Get Access</Button>
        </div>
      </div>
    );
  }
  
  handleCheckbox = (e) => {
    this.setState({
      isAccepted: e.target.checked,
    });
  }
}

export default GetAccessKeystore;