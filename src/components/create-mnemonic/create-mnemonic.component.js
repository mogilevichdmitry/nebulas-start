import React, { Component } from 'react';
import TextArea from '../../ui-kit/textarea/textarea';
import Button from '../../ui-kit/button/button';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import bip39 from 'bip39';
import './create-mnemonic.component.css';

class CreateMnemonic extends Component {
  state = {
    mnemonicPhrase: '',
  }

  componentDidMount() {
    this.setState({
      mnemonicPhrase: bip39.generateMnemonic(),
    });
  }

  render() {
    const { mnemonicPhrase } = this.state;

    return (
      <div className="create-mnemonic">
        <h2 className="create-mnemonic__title">Generate a Mnemonic Phrase</h2>
        <TextArea isReadOnly={true}>{mnemonicPhrase}</TextArea>
        <div className="create-mnemonic__footer">
          <Button
            className="create-mnemonic__generate-button"
            onClick={this.handleGenerateNew}
            isAlternative={true}
          >
            ↻ Generate New
          </Button>
          <Button onClick={this.handleCreateWallet}>Create Wallet</Button>
        </div>
      </div>
    );
  }

  handleCreateWallet = () => {
    this.props.actions.setPbKey('0x0074709077B8AE5a245E4ED161C971Dc4c3C8E2B');
    this.props.history.push('/');
  }

  handleGenerateNew = () => {
    this.setState({
      mnemonicPhrase: bip39.generateMnemonic(),
    })
  }
}

CreateMnemonic.propTypes = {
  actions: PropTypes.shape({
    setPbKey: PropTypes.func,
  }),
};

export default withRouter(CreateMnemonic);